package com.francisco.uter;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.francisco.uter.entity.Driver;
import com.francisco.uter.entity.Trip;
import com.francisco.uter.entity.Vehicle;
import com.francisco.uter.repository.DriverRepository;
import com.francisco.uter.repository.TripRepository;
import com.francisco.uter.repository.VehicleRepository;

@SpringBootApplication
public class UterApplication implements CommandLineRunner {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	DriverRepository driverRepository;

	@Autowired
	VehicleRepository vehicleRepository;

	@Autowired
	TripRepository tripRepository;

	public static void main(String[] args) {
		SpringApplication.run(UterApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Optional<Driver> driverEntity = driverRepository.findById(2L);
		Driver driver = driverEntity.get();

		logger.info("Driver id 2 -> {}", driver);

		Optional<Vehicle> vehicleEntity = vehicleRepository.findById(2L);
		Vehicle vehicle = vehicleEntity.get();

		logger.info("Vehicle id 2 -> {}", vehicle);
		
		
//		Trip trip = new Trip(vehicle, driver, new Date());
//		trip = tripRepository.save(trip);
//
//		logger.info("Trip -> {}", trip);

//		for (Trip i : driver.getTrips()) {
//			System.out.println("Driver: trip.id -> " + i.getId());
//		}

		// logger.info("Driver -> {}", driver);

//		vehicle.getTrips();
		// logger.info("Trip -> {}", vehicle);

	}

}
