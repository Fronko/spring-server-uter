package com.francisco.uter.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.francisco.uter.entity.Vehicle;
import com.francisco.uter.exception.RecordNotFoundException;
import com.francisco.uter.repository.VehicleRepository;

@Service
public class VehicleService {

	@Autowired
	VehicleRepository repository;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public List<Vehicle> getAllAvailableVehiclesForGivenDate(Date date) {
		return filterAvailableVehicles(date, getAllVehicles());
	}

	private List<Vehicle> filterAvailableVehicles(Date date, List<Vehicle> allVehicles) {
		List<Vehicle> availableVehicles = new ArrayList<Vehicle>();
		String givenDate = dateFormat.format(date);

		for (Vehicle vehicle : allVehicles) {
			if (isVehicleAvailable(givenDate, vehicle)) {
				availableVehicles.add(vehicle);
			}
		}
		return availableVehicles;
	}

	private boolean isVehicleAvailable(String givenDate, Vehicle vehicle) {
		return vehicle.getTrips().stream().noneMatch(trip -> dateFormat.format(trip.getDate()).equals(givenDate));
	}

	public List<Vehicle> getAllVehicles() {
		List<Vehicle> vehicles = repository.findAll();
		if (vehicles.size() > 0) {
			return vehicles;
		} else {
			return new ArrayList<Vehicle>();
		}
	}

	public Vehicle getVehicleById(Long id) throws RecordNotFoundException {
		Optional<Vehicle> vehicle = repository.findById(id);

		if (vehicle.isPresent()) {
			return vehicle.get();
		} else {
			throw new RecordNotFoundException("No vehicle found with id " + id);
		}
	}

	public Vehicle createVehicle(Vehicle entity) {
		entity = repository.save(entity);
		return entity;
	}

	public Vehicle updateVehicle(Vehicle entity) throws RecordNotFoundException {
		Optional<Vehicle> persistedVehicle = repository.findById(entity.getId());

		if (persistedVehicle.isPresent()) {
			entity = repository.save(entity);
		} else {
			throw new RecordNotFoundException("Error updating vehicle. No vehicle found with id " + entity.getId());
		}
		return entity;
	}

	public void deleteVehicleById(Long id) throws Exception {
		Optional<Vehicle> vehicle = repository.findById(id);

		if (vehicle.isPresent()) {
			repository.deleteById(id);
		} else {
			throw new RecordNotFoundException("Error deleting vehicle. No vehicle found with id " + id);
		}
	}
}
