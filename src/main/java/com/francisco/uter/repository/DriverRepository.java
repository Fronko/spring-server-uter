package com.francisco.uter.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.francisco.uter.entity.Driver;


@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

}
