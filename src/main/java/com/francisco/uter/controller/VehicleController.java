package com.francisco.uter.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.francisco.uter.entity.Vehicle;
import com.francisco.uter.service.VehicleService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/vehicles")
public class VehicleController {

	@Autowired
	VehicleService service;

	@RequestMapping(path="/availableVehicles", produces = "application/json")
	public List<Vehicle> getAvailableVehiclesForGivenDate(@RequestBody Date date) throws Exception {
		List<Vehicle> vehicles = service.getAllAvailableVehiclesForGivenDate(date);
		return vehicles;
	}
	
	@GetMapping(produces = "application/json")
	public List<Vehicle> getAllVehicles() throws Exception {
		List<Vehicle> vehicles = service.getAllVehicles();
		return vehicles;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Vehicle> getVehicleById(@PathVariable("id") Long id) throws Exception {
		Vehicle entity = service.getVehicleById(id);

		return new ResponseEntity<Vehicle>(entity, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/create")
	public ResponseEntity<Vehicle> createVehicle(@RequestBody Vehicle vehicle) throws Exception {
		Vehicle created = service.createVehicle(vehicle);
		return new ResponseEntity<Vehicle>(created, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<Vehicle> updateVehicle(@RequestBody Vehicle vehicle) throws Exception {
		Vehicle updatedVehicle = service.updateVehicle(vehicle);
		return new ResponseEntity<Vehicle>(updatedVehicle, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping("/delete/{id}")
	public HttpStatus deleteVehicleById(@PathVariable("id") Long id) throws Exception {
		service.deleteVehicleById(id);
		return HttpStatus.OK;
	}
}
