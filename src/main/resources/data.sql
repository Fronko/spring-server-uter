INSERT INTO 
	DRIVER (first_name, last_name, license) 
VALUES
  	('Fran', 'Nard', 'Y'),
  	('John', 'Doe', 'N');
  	
INSERT INTO 
	VEHICLE (brand, model, plate, license_required) 
VALUES
  	('Ford', 'Focus', '123-abc', 'Y'),
  	('Peugeot', '206', '456-xyz', 'N');
  	
INSERT INTO 
	TRIP (trip_date, driver_id, vehicle_id) 
VALUES
  	({ts '2012-09-17 18:47:52.69'}, 1, 1),
  	({ts '2012-09-17 18:47:52.69'}, 2, 2);