package com.francisco.uter.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.francisco.uter.entity.Trip;

public class TripSerializer extends StdSerializer<Trip> {

	private static final long serialVersionUID = 1L;

	public TripSerializer() {
		this(null);
	}

	public TripSerializer(Class<Trip> trip) {
		super(trip);
	}

	@Override
	public void serialize(Trip value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		jgen.writeStartObject();
		jgen.writeNumberField("id", value.getId());
		jgen.writeStringField("date", value.getDate().toGMTString());
		jgen.writeStringField("driver_firstName", value.getDriver().getFirstName());
		jgen.writeStringField("driver_lastName", value.getDriver().getLastName());
		jgen.writeStringField("vehicle_brand", value.getVehicle().getBrand());
		jgen.writeStringField("vehicle_model", value.getVehicle().getModel());
		jgen.writeEndObject();
		
	}
}
