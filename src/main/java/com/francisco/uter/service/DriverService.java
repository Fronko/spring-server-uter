package com.francisco.uter.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.francisco.uter.entity.Driver;
import com.francisco.uter.entity.Vehicle;
import com.francisco.uter.exception.RecordNotFoundException;
import com.francisco.uter.repository.DriverRepository;

@Service
public class DriverService {

	@Autowired
	DriverRepository repository;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public List<Driver> getAllAvailableDriversForGivenDateAndLicense(Date date, Vehicle vehicle) {
		return filterAvailableDrivers(date, vehicle);
	}

	private List<Driver> filterAvailableDrivers(Date date, Vehicle vehicle) {
		List<Driver> availableDrivers = new ArrayList<Driver>();
		String givenDate = dateFormat.format(date);

		for (Driver driver : this.getAllDrivers()) {
			if (isDriverAvailable(givenDate, driver) && hasRequiredLicense(vehicle.getLicenseRequired(), driver)) {
				availableDrivers.add(driver);
			}
		}
		return availableDrivers;
	}

	private boolean hasRequiredLicense(Character license, Driver driver) {
		return driver.getLicense().equals(license);
	}

	private boolean isDriverAvailable(String givenDate, Driver driver) {
		return driver.getTrips().stream().noneMatch(trip -> dateFormat.format(trip.getDate()).equals(givenDate));
	}

	public List<Driver> getAllDrivers() {
		List<Driver> drivers = repository.findAll();
		if (drivers.size() > 0) {
			return drivers;
		} else {
			return new ArrayList<Driver>();
		}
	}

	public Driver getDriverById(Long id) throws RecordNotFoundException {
		Optional<Driver> driver = repository.findById(id);

		if (driver.isPresent()) {
			return driver.get();
		} else {
			throw new RecordNotFoundException("No driver found for id " + id);
		}
	}

	public Driver createDriver(Driver entity) {
		entity = repository.save(entity);
		return entity;
	}

	public Driver updateDriver(Driver entity) throws RecordNotFoundException {
		Optional<Driver> persistedDriver = repository.findById(entity.getId());

		if (persistedDriver.isPresent()) {
			entity = repository.save(entity);
		} else {
			throw new RecordNotFoundException("Error updating driver. No driver found with id " + entity.getId());
		}

		return entity;
	}

	public void deleteDriverById(Long id) throws RecordNotFoundException {
		Optional<Driver> driver = repository.findById(id);

		if (driver.isPresent()) {
			repository.deleteById(id);
		} else {
			throw new RecordNotFoundException("Error deleting driver. No driver found with id " + id);
		}
	}
}
