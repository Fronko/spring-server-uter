package com.francisco.uter.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "DRIVER")
public class Driver {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "license")
	private Character license;

	@OneToMany(mappedBy="driver", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Trip> trips;

	public Driver() {
	}

	public Driver(String firstName, String lastName, Character license) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.license = license;
	}

	public void addTrip(Trip trip) {
		if (trips == null) {
			trips = new ArrayList<Trip>();
		}
		trips.add(trip);
		trip.setDriver(this);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Character getLicense() {
		return license;
	}

	public void setLicense(Character license) {
		this.license = license;
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	@Override
	public String toString() {
		return "Driver [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", license=" + license
				+ "]";
	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Driver))
            return false;
        Driver other = (Driver) obj;
        if (id != other.id)
            return false;
        return true;
    }
}
