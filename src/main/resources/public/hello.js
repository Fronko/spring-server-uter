angular.module('demo', [])
.controller('Hello', function($scope, $http) {
    $http.get('http://localhost:8080/drivers/1').
        then(function(response) {
            $scope.driver = response.data;
        });
});