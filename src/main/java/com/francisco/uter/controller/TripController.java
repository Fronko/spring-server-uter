package com.francisco.uter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.francisco.uter.entity.Driver;
import com.francisco.uter.entity.Trip;
import com.francisco.uter.entity.Vehicle;
import com.francisco.uter.service.TripService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/trips")
public class TripController {

	@Autowired
	TripService service;

	ObjectMapper objectMapper = new ObjectMapper();

	@GetMapping(produces = "application/json")
	public List<Trip> getAllTrips() throws Exception {
		List<Trip> trips = service.getAllTrips();
		return trips;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Trip> getTripById(@PathVariable("id") Long id) throws Exception {
		Trip entity = service.getTripById(id);

		return new ResponseEntity<Trip>(entity, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/create")
	public ResponseEntity<Trip> createTrip(@RequestBody ObjectNode objectNode) throws Exception {
		Trip trip = objectMapper.readValue(objectNode.get("trip").toString(), Trip.class);
		Vehicle vehicle = objectMapper.readValue(objectNode.get("vehicle").toString(), Vehicle.class);
		Driver driver = objectMapper.readValue(objectNode.get("driver").toString(), Driver.class);

		trip.addVehicle(vehicle);
		trip.addDriver(driver);

		Trip created = service.createTrip(trip);

		return new ResponseEntity<Trip>(created, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping("/delete/{id}")
	public HttpStatus deleteTripById(@PathVariable("id") Long id) throws Exception {
		service.deleteTripById(id);
		return HttpStatus.OK;
	}
}
