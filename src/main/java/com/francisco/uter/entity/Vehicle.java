package com.francisco.uter.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "VEHICLE")
public class Vehicle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "brand")
	private String brand;

	@Column(name = "model")
	private String model;

	@Column(name = "plate")
	private String plate;

	@Column(name = "license_required")
	private Character licenseRequired;

	@OneToMany(mappedBy = "vehicle", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Trip> trips;

	public Vehicle() {
	}

	public Vehicle(String brand, String model, String plate, Character licenseRequired) {
		this.brand = brand;
		this.model = model;
		this.plate = plate;
		this.licenseRequired = licenseRequired;
	}

	public void addTrip(Trip trip) {
		if (trips == null) {
			trips = new ArrayList<Trip>();
		}
		trips.add(trip);
		trip.setVehicle(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public Character getLicenseRequired() {
		return licenseRequired;
	}

	public void setLicenseRequired(Character licenseRequired) {
		this.licenseRequired = licenseRequired;
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", brand=" + brand + ", model=" + model + ", plate=" + plate + ", licenseRequired="
				+ licenseRequired + "]";
	}

}
