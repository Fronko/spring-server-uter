package com.francisco.uter.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.francisco.uter.util.TripSerializer;

@Entity
@Table(name = "TRIP")
@JsonSerialize(using = TripSerializer.class)
public class Trip {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	@JsonIgnore
	private Vehicle vehicle;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driver_id")
	@JsonIgnore
	private Driver driver;

	@Column(name = "trip_date")
	private Date date;

	public Trip() {
		this.date = new Date();
	}

	public Trip(Vehicle vehicle, Driver driver, Date date) {
		this.vehicle = vehicle;
		this.driver = driver;
		this.date = date;
	}

	public void addDriver(Driver driver) {
		driver.addTrip(this);
	}

	public void addVehicle(Vehicle vehicle) {
		vehicle.addTrip(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Trip [id=" + id + ", date=" + date + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Trip))
			return false;
		return id != null && id.equals(((Trip) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
}
