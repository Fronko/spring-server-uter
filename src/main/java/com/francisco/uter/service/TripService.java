package com.francisco.uter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.francisco.uter.entity.Trip;
import com.francisco.uter.exception.RecordNotFoundException;
import com.francisco.uter.repository.TripRepository;

@Service
public class TripService {

	@Autowired
	TripRepository tripRepository;

	public List<Trip> getAllTrips() {
		List<Trip> trips = tripRepository.findAll();
		if (trips.size() > 0) {
			return trips;
		} else {
			return new ArrayList<Trip>();
		}
	}

	public Trip getTripById(Long id) throws Exception {
		Optional<Trip> trip = tripRepository.findById(id);

		if (trip.isPresent()) {
			return trip.get();
		} else {
			throw new RecordNotFoundException("Error fetching trip. No trip found with id " + id);
		}
	}

	public Trip createTrip(Trip entity) throws Exception {
		tripRepository.save(entity);
		return entity;
	}

	public void deleteTripById(Long id) throws RecordNotFoundException {
		Optional<Trip> trip = tripRepository.findById(id);

		if (trip.isPresent()) {
			tripRepository.deleteById(id);
		} else {
			throw new RecordNotFoundException("Error deleting trip. No trip found with id " + id);
		}
	}
}
