package com.francisco.uter.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.francisco.uter.entity.Driver;
import com.francisco.uter.entity.Vehicle;
import com.francisco.uter.service.DriverService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/drivers")
public class DriverController {

	@Autowired
	DriverService service;

	ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(path = "/availableDrivers", produces = "application/json")
	public List<Driver> getAvailableDriversForGivenDateAndLicense(@RequestBody ObjectNode objectNode) throws Exception {
		Date date = objectMapper.readValue(objectNode.get("date").toString(), Date.class);
		Vehicle vehicle = objectMapper.readValue(objectNode.get("vehicle").toString(), Vehicle.class);

		List<Driver> drivers = service.getAllAvailableDriversForGivenDateAndLicense(date, vehicle);

		return drivers;
	}

	@GetMapping(produces = "application/json")
	public List<Driver> getAllDrivers() throws Exception {
		List<Driver> drivers = service.getAllDrivers();
		return drivers;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Driver> getDriverById(@PathVariable("id") Long id) throws Exception {
		Driver entity = service.getDriverById(id);

		return new ResponseEntity<Driver>(entity, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/create")
	public ResponseEntity<Driver> createDriver(@RequestBody Driver driver) throws Exception {
		Driver created = service.createDriver(driver);
		return new ResponseEntity<Driver>(created, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<Driver> updateDriver(@RequestBody Driver driver) throws Exception {
		Driver updatedDriver = service.updateDriver(driver);
		return new ResponseEntity<Driver>(updatedDriver, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping("/delete/{id}")
	public HttpStatus deleteDriverById(@PathVariable("id") Long id) throws Exception {
		service.deleteDriverById(id);
		return HttpStatus.OK;
	}
}
